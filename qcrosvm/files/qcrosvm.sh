# Copyright (c) 2024 Qualcomm Innovation Center, Inc. All rights reserved.
# SPDX-License-Identifier: BSD-3-Clause-Clear

#!/bin/sh

DISKFILE=""
GVM1NAME="roboticsvm1"
GVM2NAME="rdcapvm"
GVM1MOUNTPOINT="/run/$GVM1NAME-bootsys"
GVM2MOUNTPOINT="/run/$GVM2NAME-bootsys"
QCROSVM="qcrosvm"
GVM1PARTITION="/dev/block/bootdevice/by-name/robovm1"
GVM2PARTITION="/dev/block/bootdevice/by-name/robovm2"

echo "start from slot $SLOT_SUFFIX"
GVM1PARTITION=${GVM1PARTITION}${SLOT_SUFFIX}
GVM2PARTITION=${GVM2PARTITION}${SLOT_SUFFIX}

echo "vm1 partition path $GVM1PARTITION"
echo "vm2 partition path $GVM2PARTITION"

mount_vm_partition(){
	partition=$1
	mountpoint=$2
	echo "mount_vm_partition $partition $mountpoint"
	if [ -e "$partition" ]; then
		echo "vm partition exists."
		mkdir $mountpoint

		mount $partition $mountpoint

		return_value=$?

		if [ $return_value -eq 0 ]; then
			echo "mount successfully."
		else
			echo "mount failed."
			exit 1
		fi
	else
		echo "vm partition does not exist."
		exit 1
	fi
}

start_gvm(){
	echo "start gvm1"
	echo -n "$GVM1MOUNTPOINT/boot/" > /sys/module/firmware_class/parameters/path
	$QCROSVM --vm=$GVM1NAME --disk=${GVM1MOUNTPOINT}/system.img,label=10 &
	sleep 3

	echo "start zephyr VM, the boot image is in /firmware/image/rdcapvm*"
	$QCROSVM --vm=$GVM2NAME &
	sleep 3
}

cpu_isolate(){
	echo "cpu_isolate, shutdown cpu #6 #7"
	echo 0 >  /sys/devices/system/cpu/cpu6/online
	echo 0 >  /sys/devices/system/cpu/cpu7/online
}

mount_vm_partition $GVM1PARTITION $GVM1MOUNTPOINT
start_gvm
cpu_isolate

echo "vm start running"
