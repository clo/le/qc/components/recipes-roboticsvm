SUMMARY = "QCrosVM Support"
DESCRIPTION = "It is based on google CrosVM to launch Linux based GVM on QTI platforms."
HOMEPAGE = "https://git.codelinaro.org"
LICENSE = "BSD-3-Clause-Clear & BSD-3-Clause & (Apache-2.0 | MIT) & Apache-2.0"
LIC_FILES_CHKSUM = "file://${COREBASE}/meta/files/common-licenses/BSD-3-Clause-Clear;md5=7a434440b651f4a472ca93716d01033a \
                    file://${COREBASE}/meta/files/common-licenses/BSD-3-Clause;md5=550794465ba0ec5312d6919e203a55f9 \
                    file://${COREBASE}/meta/files/common-licenses/Apache-2.0;md5=89aea4e17d99a7cacdbeed46a0096b10 \
                    file://${COREBASE}/meta/files/common-licenses/MIT;md5=0835ade698e0bcf8506ecda2f7b4f302"

SYSTEMD_SERVICE:${PN} = "qcrosvm.service"

DEPENDS += "cargo-native libcap rust-native rust-llvm-native pkgconfig-native"


FILESEXTRAPATHS:prepend := "${WORKSPACE}:"
SRC_URI = "\
    file://vendor/qcom/opensource/crosvm-gunyah/ \
    file://external/crosvm/ \
    file://external/minijail/ \
    file://external/rust/crates/android_logger/ \
    file://external/rust/crates/simplelog/ \
    file://external/rust/crates/vmm_vhost/ \
    file://qcrosvm.service \
    file://qcrosvm.sh \
"

S = "${WORKDIR}/vendor/qcom/opensource/crosvm-gunyah"

#inherit ${@bb.utils.contains("BBFILE_COLLECTIONS", "rust-layer", "cargo", "", d)} systemd
inherit cargo systemd

CARGO_DISABLE_BITBAKE_VENDORING = "1"

do_install:append() {
    install -d ${D}${bindir}
    install -m 0755 ${WORKDIR}/qcrosvm.sh -D ${D}${bindir}/

    install -d ${D}${systemd_unitdir}/system/
    install -m 0644 ${WORKDIR}/qcrosvm.service ${D}/${systemd_unitdir}/system/qcrosvm.service
}
