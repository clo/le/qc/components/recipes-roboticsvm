inherit autotools-brokensep pkgconfig qprebuilt

DESCRIPTION      = "Build Android libvmmem test code for LE"
HOMEPAGE         = "http://support.cdmatech.com"
LICENSE          = "Qualcomm-Technologies-Inc.-Proprietary"
LIC_FILES_CHKSUM = "file://${COREBASE}/meta-qti-bsp-prop/files/qcom-licenses/\
${LICENSE};md5=92b1d0ceea78229551577d4284669bb8"

PR = "r1"

FILESEXTRAPATHS:prepend := "${WORKSPACE}/vendor/qcom/opensource/roboticsvm/:"
SRC_URI   = "file://test/sharememory"

S = "${WORKDIR}/test/sharememory"
DEPENDS += "liblog libbase libvmmem-headers linux-msm-headers libdmabufheap libvmmem"

EXTRA_OECONF = " --with-sanitized-headers=${STAGING_INCDIR}/linux-msm/usr/include \
                 --disable-static "

PACKAGES +="${PN}-test-bin"

FILES:${PN}     = "${libdir}/* ${sysconfdir}/* ${bindir}/*"
FILES:${PN}-test-bin = "${base_bindir}/*"

PACKAGE_ARCH = "${MACHINE_ARCH}"
